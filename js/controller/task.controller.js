import { TaskItem } from "../model/task.model.js";
import { generateId } from "./collection.controller.js";

const addTaskInput = document.querySelector("#addTaskInput");
const taskListEl = document.querySelector("#taskList");
const taskCountEl = document.querySelector("#taskCount");
const completeListEl = document.querySelector("#completeList");
const completeCountEl = document.querySelector("#completeCount");
const todoListEl = document.querySelector("#todoList");

const takeTaskContent = () => {
  return addTaskInput.value;
};

const createNewTask = (content) => {
  return new TaskItem(generateId(), content);
};

const renderTaskList = (taskList) => {
  let htmlMarkup = "";
  taskList.forEach((task) => {
    htmlMarkup += `
    <div class="task__item" data-id="${task.id}">
      <input id="${task.id}" type="checkbox" />
      <div class="task__detail">
        <div class="task__checkIcon" data-id="${task.id}">
          <i class="fa fa-check"></i>
        </div>
        <span>${task.content}</span>
      </div>
      <div class="task__utility">
        <i id="deleteTaskBtn" data-id="${task.id}" class="fa fa-times-circle"></i>
        <i id="adjustTaskBtn" data-id="${task.id}" class="fa fa-edit"></i>
      </div>
    </div>
    `;
  });
  taskListEl.innerHTML = htmlMarkup;
  taskCountEl.innerText = taskList.length;
};

const renderCompleteList = (completeList) => {
  let htmlMarkup = "";
  completeList.forEach((completeItem) => {
    htmlMarkup += `
      <div class="task__item" data-id="${completeItem.id}">
        <input checked disabled id="${completeItem.id}" type="checkbox" />
        <div class="task__detail">
          <div class="task__checkIcon" data-id="${completeItem.id}">
            <i class="fa fa-check"></i>
          </div>
          <span><s>${completeItem.content}</s></span>
        </div>
        <div class="task__utility">
          <i id="deleteTaskBtn" data-id="${completeItem.id}" class="fa fa-times-circle"></i>
        </div>
      </div>
    `;
  });
  completeListEl.innerHTML = htmlMarkup;
  completeCountEl.innerText = completeList.length;
};

const renderTodoList = (currentCollection) => {
  if (!currentCollection) {
    todoListEl.dataset.show = "false";
  } else {
    todoListEl.dataset.show = "true";
    document.querySelector("#collectionName").innerText =
      currentCollection.name;
    let taskList = currentCollection.todoList.filter(
      (item) => item.isComplete === false
    );
    let completeList = currentCollection.todoList.filter(
      (item) => item.isComplete === true
    );
    renderTaskList(taskList);
    renderCompleteList(completeList);
  }
};

const addTask = (newTask, currentCollection) => {
  currentCollection.todoList.push(newTask);
  addTaskInput.value = null;
};

const deleteTask = (taskId, currentCollection) => {
  return currentCollection.todoList.filter((item) => item.id != taskId);
};

const toggleTask = (taskId, currentCollection) => {
  return currentCollection.todoList.map((item) =>
    item.id == taskId
      ? { id: item.id, content: item.content, isComplete: !item.isComplete }
      : item
  );
};

const updateTask = (taskId, newContent, currentCollection) => {
  return currentCollection.todoList.map((item) =>
    item.id == taskId
      ? { id: item.id, content: newContent, isComplete: item.isComplete }
      : item
  );
};

export {
  takeTaskContent,
  createNewTask,
  renderTodoList,
  renderTaskList,
  renderCompleteList,
  addTask,
  deleteTask,
  toggleTask,
  updateTask,
};
