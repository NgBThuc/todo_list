import { selectedId } from "../app.js";
import { CollectionItem } from "../model/collection.model.js";

const collectionsListEl = document.querySelector(".collections__list");
const collectionInput = document.querySelector("#addCollectionsInput");

const renderCollectionsList = (collectionsList) => {
  let htmlMarkup = "";
  collectionsList.forEach((collection) => {
    htmlMarkup += `
      <div class="collections__item ${
        collection.id == selectedId ? "active" : ""
      }" data-id="${collection.id}">
        <span class="collections__name">${collection.name}</span>
        <span class="collections__icon">
          <i id="deleteCollectionBtn" class="fa fa-times-circle" data-id="${
            collection.id
          }"></i>
          <i id="adjustCollectionBtn" class="fa fa-edit" data-id="${
            collection.id
          }"></i>
        </span>
      </div>
    `;
  });
  collectionsListEl.innerHTML = htmlMarkup;
};

const generateId = () => {
  let id = Date.now();
  return id;
};

const takeCollectionName = () => {
  return collectionInput.value;
};

const createNewCollection = (name) => {
  return new CollectionItem(generateId(), name);
};

const addCollection = (newCollection, collectionsList) => {
  collectionsList.push(newCollection);
};

const findCurrentCollection = (collectionsList, selectedId) => {
  return collectionsList.find((collection) => collection.id == selectedId);
};

const deleteCollection = (id, collectionsList) => {
  return collectionsList.filter((collection) => collection.id != id);
};

const updateCollection = (id, newName, collectionsList) => {
  return collectionsList.map((collection) => {
    return collection.id == id
      ? { id: collection.id, name: newName, todoList: collection.todoList }
      : collection;
  });
};

export {
  renderCollectionsList,
  takeCollectionName,
  createNewCollection,
  collectionInput,
  collectionsListEl,
  generateId,
  addCollection,
  findCurrentCollection,
  deleteCollection,
  updateCollection,
};
