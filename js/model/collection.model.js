export class CollectionItem {
  constructor(id, name) {
    this.id = id;
    this.name = name;
    this.todoList = [];
  }
}
