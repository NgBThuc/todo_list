class TaskItem {
  constructor(id, content) {
    this.id = id;
    this.content = content;
    this.isComplete = false;
  }
}

export { TaskItem };
