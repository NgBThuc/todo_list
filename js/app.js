import {
  addCollection,
  collectionInput,
  collectionsListEl,
  createNewCollection,
  deleteCollection,
  findCurrentCollection,
  renderCollectionsList,
  takeCollectionName,
  updateCollection,
} from "./controller/collection.controller.js";
import {
  addTask,
  createNewTask,
  deleteTask,
  renderCompleteList,
  renderTaskList,
  renderTodoList,
  takeTaskContent,
  toggleTask,
  updateTask,
} from "./controller/task.controller.js";

const collectionsEl = document.querySelector("#collections");
const showCollectionsBtn = document.querySelector("#showCollectionsBtn");
const hideCollectionsBtn = document.querySelector("#hideCollectionsBtn");
const addCollectionsBtn = document.querySelector("#addCollectionsBtn");

const LOCAL_STORAGE_COLLECTIONS_KEY = "collectionsKey";
const LOCAL_STORAGE_SELECTED_ID = "collectionsSelectedId";

let collectionsList =
  JSON.parse(localStorage.getItem(LOCAL_STORAGE_COLLECTIONS_KEY)) || [];
let selectedId = localStorage.getItem(LOCAL_STORAGE_SELECTED_ID);
let currentCollection = findCurrentCollection(collectionsList, selectedId);

const saveToLocalStorage = (key, value) => {
  if (typeof value === "object") {
    localStorage.setItem(key, JSON.stringify(value));
  } else {
    localStorage.setItem(key, value);
  }
};

const selectElementContent = (element) => {
  window.getSelection().selectAllChildren(element);
};

const enableEditing = (element) => {
  element.setAttribute("contenteditable", true);
  element.focus();
};

const disableEditing = (element) => {
  element.removeAttribute("contenteditable");
};

const handleAddCollection = () => {
  const newCollectionName = takeCollectionName();

  if (!newCollectionName) {
    return;
  }

  const newCollection = createNewCollection(newCollectionName);
  addCollection(newCollection, collectionsList);
  collectionInput.value = null;
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
  renderCollectionsList(collectionsList);
};

const handleChoosingActiveCollection = (event) => {
  if (event.target.tagName === "I") {
    return;
  }

  let collectionsItem = event.target.closest(".collections__item");

  let currentActiveItem = collectionsListEl.querySelector(
    `[data-id="${selectedId}"]`
  );

  if (currentActiveItem) {
    currentActiveItem.classList.remove("active");
  }

  selectedId = collectionsItem.dataset.id;
  let selectedItem = collectionsListEl.querySelector(
    `[data-id="${selectedId}"]`
  );
  selectedItem.classList.add("active");
  collectionsEl.dataset.toggle = "hide";

  currentCollection = findCurrentCollection(collectionsList, selectedId);
  renderTodoList(currentCollection);
  saveToLocalStorage(LOCAL_STORAGE_SELECTED_ID, selectedId);
};

const handleDeleteCollection = (event) => {
  if (
    event.target.tagName !== "I" ||
    event.target.id !== "deleteCollectionBtn"
  ) {
    return;
  }
  collectionsList = deleteCollection(event.target.dataset.id, collectionsList);
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
  renderCollectionsList(collectionsList);
  currentCollection = findCurrentCollection(collectionsList, selectedId);
  renderTodoList(currentCollection);
};

const handleFinishEditCollection = (
  collectionNameEl,
  collectionsItem,
  temporaryCollectionsName,
  oldCollectionsName
) => {
  disableEditing(collectionNameEl);
  if (!temporaryCollectionsName) {
    collectionNameEl.innerText = oldCollectionsName;
    return;
  }
  collectionsList = updateCollection(
    collectionsItem.dataset.id,
    temporaryCollectionsName,
    collectionsList
  );
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
};

const handleUpdateCollection = (event) => {
  if (
    event.target.tagName !== "I" &&
    event.target.id !== "adjustCollectionBtn"
  ) {
    return;
  }

  let collectionsItem = event.target.closest(".collections__item");
  let collectionNameEl = collectionsItem.querySelector(".collections__name");
  let temporaryCollectionsName = "";
  let oldCollectionsName = collectionNameEl.innerText;

  enableEditing(collectionNameEl);
  selectElementContent(collectionNameEl);

  collectionNameEl.addEventListener("input", () => {
    temporaryCollectionsName = collectionNameEl.innerText;
  });

  collectionNameEl.addEventListener("focusout", () => {
    handleFinishEditCollection(
      collectionNameEl,
      collectionsItem,
      temporaryCollectionsName,
      oldCollectionsName
    );
  });

  collectionNameEl.addEventListener("keydown", (event) => {
    if (event.key !== "Enter") {
      return;
    }
    handleFinishEditCollection(
      collectionNameEl,
      collectionsItem,
      temporaryCollectionsName,
      oldCollectionsName
    );
  });
};

const handleAddTask = () => {
  let newTaskContent = takeTaskContent();

  if (!newTaskContent) {
    return;
  }

  let newTask = createNewTask(newTaskContent);
  let currentCollection = findCurrentCollection(collectionsList, selectedId);
  addTask(newTask, currentCollection);
  renderTodoList(currentCollection);
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
};

const handleDeleteTask = (event) => {
  if (event.target.tagName !== "I" || event.target.id !== "deleteTaskBtn") {
    return;
  }
  currentCollection.todoList = deleteTask(
    event.target.dataset.id,
    currentCollection
  );
  renderTodoList(currentCollection);
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
};

const handleToggleTask = (event) => {
  if (!event.target.closest(".task__checkIcon")) {
    return;
  }
  currentCollection.todoList = toggleTask(
    event.target.closest(".task__checkIcon").dataset.id,
    currentCollection
  );
  renderTodoList(currentCollection);
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
};

const handleFinishEditTask = (
  taskContentEl,
  taskItemEl,
  temporaryTaskContent,
  oldTaskContent
) => {
  disableEditing(taskContentEl);
  if (!temporaryTaskContent) {
    taskContentEl.innerText = oldTaskContent;
    return;
  }
  currentCollection.todoList = updateTask(
    taskItemEl.dataset.id,
    temporaryTaskContent,
    currentCollection
  );
  saveToLocalStorage(LOCAL_STORAGE_COLLECTIONS_KEY, collectionsList);
};

const handleUpdateTask = (event) => {
  if (event.target.tagName !== "I" || event.target.id !== "adjustTaskBtn") {
    return;
  }

  const taskItemEl = event.target.closest(".task__item");
  const taskContentEl = taskItemEl.querySelector("span");
  let temporaryTaskContent = "";
  let oldTaskContent = taskContentEl.innerText;

  enableEditing(taskContentEl);
  selectElementContent(taskContentEl);

  taskContentEl.addEventListener("input", () => {
    temporaryTaskContent = taskContentEl.innerText;
  });

  taskContentEl.addEventListener("focusout", () => {
    handleFinishEditTask(
      taskContentEl,
      taskItemEl,
      temporaryTaskContent,
      oldTaskContent
    );
  });

  taskContentEl.addEventListener("keydown", (event) => {
    if (event.key !== "Enter") {
      return;
    }
    handleFinishEditTask(
      taskContentEl,
      taskItemEl,
      temporaryTaskContent,
      oldTaskContent
    );
  });
};

const handleSortTask = (event) => {
  let targetEl = event.target;

  if (targetEl.tagName !== "I" || !targetEl.classList.contains("sortBtn")) {
    return;
  }

  let dataList = currentCollection.todoList.filter((item) => {
    return targetEl.dataset.list === "task"
      ? item.isComplete === false
      : item.isComplete === true;
  });

  let sortedDataList = dataList.sort((a, b) => {
    return targetEl.dataset.sortType === "ascending"
      ? a.content.localeCompare(b.content)
      : b.content.localeCompare(a.content);
  });

  targetEl.dataset.list === "task"
    ? renderTaskList(sortedDataList)
    : renderCompleteList(sortedDataList);
};

addCollectionsBtn.addEventListener("click", handleAddCollection);
document.addEventListener("keydown", (event) => {
  if (event.key !== "Enter") {
    return;
  }
  handleAddCollection();
});

collectionsListEl.addEventListener("click", (event) => {
  handleChoosingActiveCollection(event);
});

collectionsListEl.addEventListener("click", (event) => {
  handleDeleteCollection(event);
});

collectionsListEl.addEventListener("click", (event) => {
  handleUpdateCollection(event);
});

document.querySelector("#addTaskBtn").addEventListener("click", handleAddTask);
document.addEventListener("keydown", (event) => {
  if (event.key !== "Enter") {
    return;
  }
  handleAddTask();
});

document.querySelector("#todoList").addEventListener("click", (event) => {
  handleDeleteTask(event);
});

document.querySelector("#todoList").addEventListener("click", (event) => {
  handleToggleTask(event);
});

document.querySelector("#taskList").addEventListener("click", (event) => {
  handleUpdateTask(event);
});

document.querySelector("#todoList").addEventListener("click", (event) => {
  handleSortTask(event);
});

renderCollectionsList(collectionsList);
renderTodoList(findCurrentCollection(collectionsList, selectedId));

showCollectionsBtn.addEventListener("click", () => {
  collectionsEl.dataset.toggle = "show";
});

hideCollectionsBtn.addEventListener("click", () => {
  collectionsEl.dataset.toggle = "hide";
});

export { selectedId };
